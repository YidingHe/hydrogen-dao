package com.hyd.daotest.bean;

/**
 * todo: description
 *
 * @author yiding.he
 */
public class Admin {

    private String userName;

    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
