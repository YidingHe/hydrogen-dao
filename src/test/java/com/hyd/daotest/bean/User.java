package com.hyd.daotest.bean;

import com.hyd.dao.Table;

import java.util.Date;

/**
 * @author yiding.he
 */
@Table(name = "USERS")
public class User {

    private Long id;

    private String username;

    private String password;

    private Date registerTime;

    private Long roleId;

    private Date birthday;

    private int loginCount;

    public User() {
    }

    public User(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(Long id, String username, String password, Date registerTime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.registerTime = registerTime;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(int loginCount) {
        this.loginCount = loginCount;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", registerTime=" + registerTime +
                ", roleId=" + roleId +
                ", birthday=" + birthday +
                ", loginCount=" + loginCount +
                '}';
    }
}
